const{createApp} = Vue;
createApp({
    data(){
        return{
            randomindex: 0,
            randomindexinternet: 0,

            //vetor de imagens locais
            imagenslocais: [
                './imagens/lua.jpg',
                './imagens/SENAI_logo.png',
                './imagens/sol.jpg' 
            ],
            imagensinternet: [
                'https://images.emojiterra.com/google/noto-emoji/v2.034/512px/1f3e0.png',
                'https://tswbike.com/wp-content/uploads/2022/12/22307-22308-22309-22310-FullQuest-SR-PR-PT-2-.png',
                'https://conteudo.imguol.com.br/c/esporte/1e/2022/12/06/cristiano-ronaldo-durante-a-partida-entre-portugal-e-suica-pelas-oitavas-de-final-da-copa-do-mundo-do-qatar-1670365258264_v2_450x600.jpg',
            ],

        }//fim return
    },// fim data
    computed:{
        randomimagem()
        {
            return this.imagenslocais[this.randomindex];
        },// fim randomimagem
        randomimageminternet()
        {
            return this.imagensinternet[this.randomindexinternet];
        } 
    },// fim comput
    methods:{
        getrandomimagem()
        {
            this.randomindex= Math.floor(Math.random()*this.imagenslocais.length)
            console.log(this.imagenslocais[this.randomindex]);

            this.randomindexinternet=Math.floor(Math.random()*this.imagensinternet.length);
        }
    }//fim methods:

}).mount("#app");
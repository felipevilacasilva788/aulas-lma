//processamento dos dados do formulario""index html"

//acessando os elementos formulario do html

const formulario = document.getElementById("formulario1");

//adiciona um ouvinte de eventos a um elemento HTML
formulario.addEventListener ("submit", function (evento)
{
    evento.preventDefault();
    //previne o compartamento padrao de um elemento HTML em resposta a um evento
    //constantes para tratar os dados recebidos dos elementos do formulario 
    const nome = document.getElementById("nome").value;
    const email =  document.getElementById("email").value;

    //exibe um alerta com os dados 
    //alert(`Nome: ${nome} E-mail ${email}`);

    const updateResultado = document.getElementById("resultado");

    updateResultado.value = `Nome: ${nome} E-mail: ${email}`;
    updateResultado.style.width = updateResultado.scrollWidth + "px";

});
